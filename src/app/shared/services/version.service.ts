import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VersionService {

  public version = 1;

  constructor() { }

  /**
   * @description met à jour la version en l'incrémentant de 1
   */
  upgrade() {
    this.version ++;
  }

}
