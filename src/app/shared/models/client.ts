import { ClientI } from '../interfaces/client-i';
import { StateClient } from '../enums/state-client.enum';

export class Client implements ClientI {
  id: number;
  name: string;
  email: string;
  profil: string;
  state: StateClient;

  // permet d'instancier partiellement un objet de type Client
  constructor(obj?: Partial<Client>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}
