import { PrestationI } from '../interfaces/prestation-i';
import { State } from '../enums/state.enum';

export class Prestation implements PrestationI {
  id: number; typePresta: string;
  client: string;
  tjmHt = 1200;
  tva = 20;
  nbJours = 1;
  state = State.OPTION;
  comment: string;


  // permet d'instancier partiellement un objet de type Prestation
  constructor(obj?: Partial<Prestation>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }

  // -- Fonctions publiques -- //

  /**
   * @description retourne le total Ht
   */
  totalHt(): number {
    return this.tjmHt * this.nbJours;
  }

  /**
   * @description retourne le total Ttc
   */
  totalTtc(): number {
    return this.tva <= 0. ? this.totalHt() : this.totalHt() * (1 + this.tva / 100);
  }

  // -- Fin des fonctions publiques -- //
}
