import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TotalPipe } from './pipes/total.pipe';
import { StateDirective } from './directives/state.directive';
import { TableauLightComponent } from './components-shared/tableau-light/tableau-light.component';
import { TableauDarkComponent } from './components-shared/tableau-dark/tableau-dark.component';
import { StateClientDirective } from './directives/state-client.directive';
import { TemplatesModule } from '../templates/templates.module';
import { ButtonRedirectionComponent } from './components-shared/button-redirection/button-redirection.component';
import { RouterModule } from '@angular/router';
import { CommentComponent } from './components-shared/comment/comment.component';
import { DetailComponent } from './components-shared/detail/detail.component';
import { SousNavComponent } from './components-shared/sous-nav/sous-nav.component';



@NgModule({
  declarations: [
    TotalPipe,
    StateDirective,
    TableauLightComponent,
    TableauDarkComponent,
    StateClientDirective,
    ButtonRedirectionComponent,
    CommentComponent,
    DetailComponent,
    SousNavComponent
  ],
  imports: [
    CommonModule,
    TemplatesModule,
    RouterModule
  ],
  exports: [
    TotalPipe,
    StateDirective,
    TableauLightComponent,
    TableauDarkComponent,
    ButtonRedirectionComponent,
    StateClientDirective,
    TemplatesModule,
    CommentComponent,
    DetailComponent,
    SousNavComponent]
})
export class SharedModule { }
