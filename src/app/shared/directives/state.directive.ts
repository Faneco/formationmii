import { Directive, Input, OnInit, HostBinding, SimpleChanges, OnChanges } from '@angular/core';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnInit, OnChanges {

  @Input() appState: any;

  // représentation de la balise sur laquelle la directive est appelée
  @HostBinding('class') tdClass: string;

  constructor() { }

  ngOnInit() {
    this.tdClass = this.formatClass(this.appState);
  }

  ngOnChanges() {
    this.tdClass = this.formatClass(this.appState);
  }

  /**
   * @description permet de changer la classe de la balise
   */
  private formatClass(state: any): string {
    return `state-${state.normalize('NFD').replace(/[\u0300-\u036f\s]/g, '').toLowerCase()}`;
  }

}
