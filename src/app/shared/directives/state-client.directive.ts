import { Directive, HostBinding, Input, OnInit, OnChanges } from '@angular/core';

@Directive({
  selector: '[appStateClient]'
})
export class StateClientDirective implements OnInit, OnChanges {

  @Input() appStateClient: any;

  // représentation de la balise sur laquelle la directive est appelée
  @HostBinding('class') btnClass: string;

  constructor() { }

  ngOnInit() {
    this.btnClass = this.formatClass(this.appStateClient);
  }

  ngOnChanges() {
    this.btnClass = this.formatClass(this.appStateClient);
  }

  /*
  * @description permet de changer la classe de la balise
  */
  formatClass(state: any) {
    return state === 'Actif' ? 'btn btn-success' : 'btn btn-danger';
  }

}
