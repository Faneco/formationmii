import { Pipe, PipeTransform } from '@angular/core';

/**
 * @description Ce pipe permet de renvoyer une valeur en appelant une fonction donnée en argument pour ne pas qu'il y ait de multiples appels dans le html
 */
@Pipe({
  name: 'total'
})
export class TotalPipe implements PipeTransform {
    transform(value: any, ...args: any[]): any {
    if (value) {
      if (args && args.length > 0) {
        return value[args[0]].call(value);
      }
    }
    return null;
  }
}
