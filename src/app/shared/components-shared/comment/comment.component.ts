import { Component, OnInit } from '@angular/core';
import { PrestationService } from 'src/app/prestations/services/prestation.service';
import { Prestation } from '../../models/prestation';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  public firstPresta$: Subject<Prestation>;

  constructor(private prestationService: PrestationService) { }

  ngOnInit() {
    this.firstPresta$ = this.prestationService.firstPresta$;
  }

}
