import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-button',
  templateUrl: './button-redirection.component.html',
  styleUrls: ['./button-redirection.component.scss']
})
export class ButtonRedirectionComponent implements OnInit {

  @Input() label: string;
  @Input() route: string;
  @Input() href: string;
  @Input() click: boolean;
  @Output() clicked: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.clicked.emit('Coucou');
  }

}
