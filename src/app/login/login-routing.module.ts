import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PageLoginComponent } from './pages/page-login/page-login.component';

const routesLogin: Routes = [
  {path: '', component: PageLoginComponent}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routesLogin)
  ]
})
export class LoginRoutingModule { }
