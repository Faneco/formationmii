import { NgModule } from '@angular/core';
import { PageLoginComponent } from './pages/page-login/page-login.component';
import { LoginRoutingModule } from './login-routing.module';



@NgModule({
  declarations: [PageLoginComponent],
  imports: [
    LoginRoutingModule
  ]
})
export class LoginModule { }
