import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Prestation } from 'src/app/shared/models/prestation';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { State } from 'src/app/shared/enums/state.enum';

@Injectable({
  providedIn: 'root'
})
export class PrestationService {

  private pCollection$: Observable<Prestation[]>;
  private urlApi = environment.urlApi;

  // BehaviourSubject permet de stocker les données pour pouvoir récupérer les données
  // comme par exemple récupérer les données au destroy du component et qu'on revient dessus
  public firstPresta$: BehaviorSubject<Prestation> = new BehaviorSubject(null);

  // observable froid => multicast (multi-insance)
  public obs$ = new Observable((observers) => {
    observers.next(['chris', 'antony', 'cedric']);
  });
  // observable froid => multicast (multi-insance)
  public obs2$ = new Observable((observers) => {
    observers.next(Math.random());
  });
  // observable chaud => unicast (une seule instance)
  public subject$ = new Subject();

  constructor(private http: HttpClient) {
    this.initData();

    // écoute de l'obs2 multicast, pas la même valeur que le subscribe du bas car c'est une nouvelle instance
    this.obs2$.subscribe((data) => {
      // console.log(data);
    });
    // écoute de l'obs2 multicast pas la même valeur que le subscribe du haut, car c'est une nouvelle instance
    this.obs2$.subscribe((data) => {
      // console.log(data);
    });
    // écoute de subject unicast // Observable chaud, même valeur que le subscribe du bas
    this.subject$.subscribe((data) => {
      // console.log(data);
    });
    // écoute de subject unicast // Observable chaud, même valeur que le subscribe du haut
    this.subject$.subscribe((data) => {
      // console.log(data);
    });
    // passage de valeur au subject, tous les subscribe auront la même valeur
    this.subject$.next(Math.random());
  }

  // get collections
  public get collection(): Observable<Prestation[]> {
    return this.pCollection$;
  }

  // set collections
  public set collection(col: Observable<Prestation[]>) {
    this.pCollection$ = col;
  }

  // update item in collection
  public update(item: Prestation, state?: State) {
    // copie sans référence grâce au destructuring
    const obj = { ...item };
    if (state) {
      obj.state = state;
    }
    return this.http.patch(`${this.urlApi}/prestations/${obj.id}`, obj);
  }

  // add item in collections

  public add(item: Prestation) {
    return this.http.post(`${this.urlApi}/prestations/`, item);
  }

  // delete item in collection
  public delete(id: number) {
    return this.http.delete(`${this.urlApi}/prestations/${id}`);
  }

  public getItemById(id: any): Observable<Prestation>{
    return this.http.get<Prestation>(`${this.urlApi}/prestations/${id}`);
  }

  /**
   * @description mise à jour de la collection
   */
  public initData() {
    this.collection = this.http.get<Prestation[]>(`${this.urlApi}/prestations`).pipe(
      map((tab) => {
        if (tab && tab.length > 0) {
          // on envoie à l'observable la donnée que l'on souhaite
          this.firstPresta$.next(tab[0]);
        }
        return tab.map((obj) => {
          return new Prestation(obj);
        });
      })
    );
  }
}
