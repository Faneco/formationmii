import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Prestation } from 'src/app/shared/models/prestation';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { State } from '../../../shared/enums/state.enum';

@Component({
  selector: 'app-form-prestation',
  templateUrl: './form-prestation.component.html',
  styleUrls: ['./form-prestation.component.scss']
})
export class FormPrestationComponent implements OnInit {

  @Input() init: Prestation;
  @Output() submited: EventEmitter<any> = new EventEmitter();

  public states = State;
  public formGroup: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
  }

  /**
   * @description au clic du formulaire
   */
  public onSubmit() {
    this.submited.emit(this.formGroup.value);
  }
  /**
   * @description création du formulaire
   */
  private createForm() {
    this.formGroup = this.fb.group({
      typePresta: [this.init.typePresta, Validators.required],
      client: [this.init.client, Validators.compose([Validators.required, Validators.minLength(2)])],
      tjmHt: [this.init.tjmHt],
      tva: [this.init.tva],
      nbJours: [this.init.nbJours],
      state: [this.init.state],
      comment: [this.init.comment]
    });
  }

}
