import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagePrestationComponent } from './pages/page-prestation/page-prestation/page-prestation.component';
import { PrestationsRoutingModule } from './prestations-routing.module';
import { SharedModule } from '../shared/shared.module';
import { PageAddPrestationComponent } from './pages/page-prestation/page-add-prestation/page-add-prestation.component';
import { FormPrestationComponent } from './components/form-prestation/form-prestation.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PageEditPrestationComponent } from './pages/page-prestation/page-edit-prestation/page-edit-prestation.component';

@NgModule({
  declarations: [PagePrestationComponent, PageAddPrestationComponent, FormPrestationComponent, PageEditPrestationComponent],
  imports: [
    PrestationsRoutingModule,
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ]
})
export class PrestationsModule { }
