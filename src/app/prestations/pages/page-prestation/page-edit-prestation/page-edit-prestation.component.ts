import { Component, OnInit } from '@angular/core';
import { Prestation } from 'src/app/shared/models/prestation';
import { PrestationService } from 'src/app/prestations/services/prestation.service';
import { ActivatedRoute, Router,  ParamMap  } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { isNgTemplate } from '@angular/compiler';

@Component({
  selector: 'app-page-edit-prestation',
  templateUrl: './page-edit-prestation.component.html',
  styleUrls: ['./page-edit-prestation.component.scss']
})
export class PageEditPrestationComponent implements OnInit {

  public title: string;
  public subtitle: string;
  public init$: Observable<Prestation>;
  private id: string;

  constructor(private prestationService: PrestationService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    // Méthode 1 subscribe dans un subscribe
    this.route.paramMap.subscribe(
      (data: ParamMap) => {
        this.prestationService.getItemById(data.get('id')).subscribe(item => {
          console.log(item);
        });
      }
    );

    // méthode 2 quand il y a un subscribe dans un subscribe

    this.init$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        this.id = params.get('id');
        return this.prestationService.getItemById(params.get('id'));
      })
    );


    this.route.data.subscribe(
      (datas) => {
        this.title = datas.title;
        this.subtitle = datas.subtitle;
      }
    );

  }


  /**
   * @description au clic du bouton submit du formulaire
   */
  public updateItem(item: any) {
    item.id = this.id;
    this.prestationService.update(item).subscribe((data) => {
      this.router.navigate(['prestations']);
    });
  }

}
