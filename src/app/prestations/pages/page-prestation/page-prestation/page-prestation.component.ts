import { Component, OnInit, OnDestroy } from '@angular/core';
import { PrestationService } from '../../../services/prestation.service';
import { Observable, Subscription } from 'rxjs';
import { Prestation } from 'src/app/shared/models/prestation';
import { State } from 'src/app/shared/enums/state.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-page-prestation',
  templateUrl: './page-prestation.component.html',
  styleUrls: ['./page-prestation.component.scss']
})
export class PagePrestationComponent implements OnInit {

  public title: string;
  public subtitle: string;
  public titleButton: string;
  public redirection: string;
  public faTrashAlt = faTrashAlt;
  public faEdit = faEdit;

  public collection$: Observable<Prestation[]>;
  public collection: Prestation[];
  public states = State;
  public sousRoutes: { route: any, label: string }[];

  public headers = [
    'Type',
    'Client',
    'Nombre de jours',
    'Tjm HtT',
    'Total HT',
    'Total TTC',
    'State',
    'Action',
    'Supprimer'
  ];

  public obs: string[];

  constructor(private prestationService: PrestationService, private route: ActivatedRoute, private router: Router) { }

  /**
   * @description init
   */
  ngOnInit() {

    this.sousRoutes = [
      { route: 'comment', label: 'Commentaires' },
      { route: 'detail', label: 'Détail de la prestation' }
    ];

    this.route.data.subscribe(
      (datas) => {
        this.title = datas.title;
        this.subtitle = datas.subtitle;
      }
    );

    this.titleButton = 'Ajouter une prestation';
    this.redirection = 'prestation/add';

    this.collection$ = this.prestationService.collection;
    // this.prestationService.collection.subscribe(
    //   (data) => {
    //     this.collection = data;
    //   }, (err) => {
    //     console.log(err);
    //   }, () => {
    //     console.log('Données complètes');
    //   }
    // );
    // this.sub = this.prestationService.obs$.subscribe((data: string[]) => {
    //   this.obs = data;
    // });
  }

  /**
   * @description change l'état dans la base de données
   */
  changeState(item: Prestation, event) {
    this.prestationService.update(item, event.target.value).subscribe(
      (updateItem: Prestation) => {
        item.state = updateItem.state;
      }
    );
  }

  /**
   * @description au clic du bouton
   */
  popin(str: string) {
    console.log(str);
  }

  /**
   * @descrition permet de supprimer une prestation en appelant le service
   */
  deletePresta(id: number) {
    console.log(id);
    this.prestationService.delete(id).subscribe(
      (res) => {
        // this.prestationService.initData();
        console.log(res);
      }
    )
  }

  /**
   * @description enregistre la prestation courante
   */
  setFirstPresta(item: Prestation) {
    this.prestationService.firstPresta$.next(item);
  }

  /**
   * @description redirige vers la page édition de
   */
  editPresta(item: Prestation) {
    this.router.navigate(['prestations/edit', item.id]);
  }

}
