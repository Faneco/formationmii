import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Prestation } from 'src/app/shared/models/prestation';
import { FormGroup } from '@angular/forms';
import { PrestationService } from 'src/app/prestations/services/prestation.service';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-page-add-prestation',
  templateUrl: './page-add-prestation.component.html',
  styleUrls: ['./page-add-prestation.component.scss']
})
export class PageAddPrestationComponent implements OnInit {

  public title: string;
  public subtitle: string;
  public init = new Prestation();

  constructor(private prestationService: PrestationService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.data.subscribe(
      (datas) => {
        this.title = datas.title;
        this.subtitle = datas.subtitle;
      }
    );
  }


  /**
   * @description au clic du bouton submit du formulaire
   */
  public onSubmit(formValues: any) {
    this.prestationService.add(formValues).subscribe(
      (data) => {
        // redirection absolue
        this.router.navigate(['prestations']);
        // redirection relative
        // this.router.navigate(['../'], {relativeTo: this.route});
      });
  }

}
