import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagePrestationComponent } from './pages/page-prestation/page-prestation/page-prestation.component';
import { PageAddPrestationComponent } from './pages/page-prestation/page-add-prestation/page-add-prestation.component';
import { CommentComponent } from '../shared/components-shared/comment/comment.component';
import { DetailComponent } from '../shared/components-shared/detail/detail.component';
import { PageEditPrestationComponent } from './pages/page-prestation/page-edit-prestation/page-edit-prestation.component';


const routesPrestations: Routes = [
  {
    path: '',
    component: PagePrestationComponent,
    data: { title: 'Prestations', subtitle: 'Toutes les prestations' },
    children: [
      { path: '', redirectTo: 'comment', pathMatch: 'full', },
      { path: 'comment', component: CommentComponent  },
      { path: 'detail', component: DetailComponent }
    ]
  },
  { path: 'add', component: PageAddPrestationComponent, data: { title: 'Prestations', subtitle: 'Ajouter une prestation' } },
  { path: 'edit/:id', component: PageEditPrestationComponent, data: { title: 'Prestations', subtitle: 'Editer une prestation' } },
];

@NgModule({
  imports: [
    RouterModule.forChild(routesPrestations)
  ],
  exports: [
    RouterModule
  ]
})
export class PrestationsRoutingModule { }
