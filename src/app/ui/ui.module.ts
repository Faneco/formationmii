import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoderbaseUiModule } from '@coderbase/ui';

import { UiComponent } from './componant/ui/ui.component';
import { HeaderComponent } from './componant/header/header.component';
import { NavComponent } from './componant/nav/nav.component';
import { FooterComponent } from './componant/footer/footer.component';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


@NgModule({
  declarations: [UiComponent, HeaderComponent, NavComponent, FooterComponent],
  exports: [UiComponent],
  imports: [
    CommonModule,
    CoderbaseUiModule,
    RouterModule,
    FontAwesomeModule
  ]
})
export class UiModule { }
