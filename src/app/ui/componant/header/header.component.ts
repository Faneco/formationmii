import { Component, OnInit } from '@angular/core';
import { VersionService } from 'src/app/shared/services/version.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public version: number;
  public title: string;

  constructor(public vs: VersionService) { }

  ngOnInit() {
    this.title = 'Application formation M2i';
    this.version = this.vs.version;
  }

}
