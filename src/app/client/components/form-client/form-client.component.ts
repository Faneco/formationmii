import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { StateClient } from '../../../shared/enums/state-client.enum';
import { Client } from 'src/app/shared/models/client';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-client',
  templateUrl: './form-client.component.html',
  styleUrls: ['./form-client.component.scss']
})
export class FormClientComponent implements OnInit {

  @Input() init: Client;
  @Output() submited: EventEmitter<any> = new EventEmitter();
  public states = StateClient;
  public form: FormGroup;
  constructor(private fb: FormBuilder) { }
  ngOnInit() {
    this.createForm();
  }
  private createForm() {
    this.form = this.fb.group({
      name: [this.init.name, Validators.required],
      email: [this.init.email],
      profil: [this.init.profil],
      state: [this.init.state, Validators.required]
    });
  }

  public onClick() {
    this.submited.emit(this.form.value);
  }
}
