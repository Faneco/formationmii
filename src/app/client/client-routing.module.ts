import { NgModule } from '@angular/core';
import { PageClientComponent } from './pagesClient/page-client/page-client.component';
import { Routes, RouterModule } from '@angular/router';
import { PageAddClientComponent } from './pagesClient/page-add-client/page-add-client.component';


const routesClient: Routes = [
  { path: '', component: PageClientComponent , data: {title: 'Clients', subtitle: 'Tous les clients'}},
  { path: 'add', component: PageAddClientComponent , data: {title: 'Clients', subtitle: 'Ajouter un client'}}
];

@NgModule({
  imports: [
    RouterModule.forChild(routesClient)
  ]
})
export class ClientRoutingModule { }
