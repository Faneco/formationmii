import { Injectable } from '@angular/core';
import { Client } from 'src/app/shared/models/client';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { StateClient } from 'src/app/shared/enums/state-client.enum';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  public cCollection$: Observable<Client[]>;
  private urlApi = environment.urlApi;

  constructor(private http: HttpClient) {
    this.collectionClient = this.http.get<Client[]>(`${this.urlApi}/clients`).pipe(
      map((array) => {
        return array.map(obj => {
          return new Client(obj);
        });
      })
    );
  }

  // get collections
  public get collectionClient(): Observable<Client[]> {
    return this.cCollection$;
  }

  // set collections
  public set collectionClient(col: Observable<Client[]>) {
    this.cCollection$ = col;
  }

  // update item in collection
  public update(item: Client, state: StateClient) {
    // copie sans référence grâce au destructuring
    const obj = { ...item };
    obj.state = state;

    return this.http.patch(`${this.urlApi}/clients/${obj.id}`, obj);
  }

  /**
   * @description ajoute un client dans la base de données
   */
  public add(item: Client) {
    let obj = {...item};
    obj.profil = item.profil.replace('C:\\fakepath\\', '../../../assets/');
    return this.http.post(`${this.urlApi}/clients/`, obj);
  }

}
