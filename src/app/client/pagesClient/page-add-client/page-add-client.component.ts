import { Component, OnInit, Input } from '@angular/core';
import { Client } from 'src/app/shared/models/client';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-page-add-client',
  templateUrl: './page-add-client.component.html',
  styleUrls: ['./page-add-client.component.scss']
})
export class PageAddClientComponent implements OnInit {

  public title: string;
  public subtitle: string;
  public init = new Client();

  constructor(private route: ActivatedRoute, private clientService: ClientService, private router: Router) { }

  ngOnInit() {
    this.route.data.subscribe(
      (datas) => {
        this.title = datas.title;
        this.subtitle = datas.subtitle;
      }
    );
  }

  /**
   * @description appelle le service au clic du bouton du formulaire
   */
  onSubmit(value: any) {
    this.clientService.add(value).subscribe(
      (res) => {
        console.log(res);
        this.router.navigate(['client']);
      }
    );
  }
}
