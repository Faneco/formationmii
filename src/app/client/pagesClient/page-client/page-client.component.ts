import { Component, OnInit, OnDestroy } from '@angular/core';
import { Client } from 'src/app/shared/models/client';
import { ClientService } from '../../services/client.service';
import { StateClient } from 'src/app/shared/enums/state-client.enum';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-page-client',
  templateUrl: './page-client.component.html',
  styleUrls: ['./page-client.component.scss']
})
export class PageClientComponent implements OnInit, OnDestroy {

  public title: string;
  public subtitle: string;
  public titleButton: string;
  public redirection: string;

  public collectionClient: Array<Client>;

  public states = StateClient;
  public headers = [
    'profil',
    'type',
    'name',
    'email',
    'etat',
    'state'
  ];

  private sub: Subscription;

  constructor(private clientService: ClientService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(
      (datas) => {
        this.title = datas.title;
        this.subtitle = datas.subtitle;
      }
    );
    this.titleButton = 'Ajouter un client';
    this.redirection = 'client/add';

    this.sub = this.clientService.collectionClient.subscribe((data) => {
      this.collectionClient = data;
    });
  }

  /**
   * @description détruit l'observable à la desctruction du component
   */
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  /*
  * @description change l'état dans la base de données
  */
  changeState(item: Client, event) {
    this.clientService.update(item, event.target.value).subscribe(
      (updateItem: Client) => {
        item.state = updateItem.state;
      }
    );
  }

  onClickButtonState() {

  }

}
