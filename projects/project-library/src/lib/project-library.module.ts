import { NgModule } from '@angular/core';
import { ProjectLibraryComponent } from './project-library.component';



@NgModule({
  declarations: [ProjectLibraryComponent],
  imports: [
  ],
  exports: [ProjectLibraryComponent]
})
export class ProjectLibraryModule { }
