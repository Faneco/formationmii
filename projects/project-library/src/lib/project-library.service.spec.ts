import { TestBed } from '@angular/core/testing';

import { ProjectLibraryService } from './project-library.service';

describe('ProjectLibraryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjectLibraryService = TestBed.get(ProjectLibraryService);
    expect(service).toBeTruthy();
  });
});
